//
//  TCDemoViewController.h
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCStepperCell.h"
#import "TCSliderCell.h"
#import "TCSwitchCell.h"
#import "TCSegmentedCell.h"
#import "TCTextFieldCell.h"
#import "TCMapCell.h"
#import "TCPagedCell.h"

@interface TCDemoViewController : UITableViewController <TCStepperCellDelegate, TCSliderCellDelegate, TCSwitchCellDelegate, TCSegmentedCellDelegate, TCTextFieldCellDelegate> {
    NSArray *identifiers;
}

@end
