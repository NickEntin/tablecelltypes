//
//  TCSegmentedCell.m
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import "TCSegmentedCell.h"

@implementation TCSegmentedCell

@synthesize delegate, value, titleLabel, segment;

- (IBAction)segmentValueChanged:(UISegmentedControl *)sender {
    int oldValue = self.value;
    self.value = sender.selectedSegmentIndex;
    if ([delegate respondsToSelector:@selector(cell:hasChangedSegmentValueTo:from:)]) {
        [delegate cell:self hasChangedStringValueTo:[self.segment titleForSegmentAtIndex:self.value] from:[self.segment titleForSegmentAtIndex:oldValue]];
    }
    if ([delegate respondsToSelector:@selector(cell:hasChangedIntValueTo:from:)]) {
        [delegate cell:self hasChangedIntValueTo:self.value from:oldValue];
    }
}

@end
