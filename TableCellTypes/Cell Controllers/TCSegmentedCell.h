//
//  TCSegmentedCell.h
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TCSegmentedCellDelegate <NSObject>
@optional
- (void)cell:(UITableViewCell *)cell hasChangedIntValueTo:(int)newValue from:(int)oldValue;
- (void)cell:(UITableViewCell *)cell hasChangedStringValueTo:(NSString *)newValue from:(NSString *)oldValue;
@end

@interface TCSegmentedCell : UITableViewCell

// Class Properties
@property (nonatomic, retain) id<TCSegmentedCellDelegate>   delegate;
@property (nonatomic, assign) int                           value;

// Interface Elements
@property (nonatomic, retain) IBOutlet UILabel              *titleLabel;
@property (nonatomic, retain) IBOutlet UISegmentedControl   *segment;

// Interface Methods
- (IBAction)segmentValueChanged:(UISegmentedControl *)sender;

@end
