//
//  TCStepperCell.m
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import "TCStepperCell.h"

@implementation TCStepperCell

@synthesize delegate, value, titleLabel, valueLabel, stepper;

- (IBAction)stepperValueChanged:(UIStepper *)sender {
    double oldValue = self.value;
    self.value = sender.value;
    
    // Set value label to show int version of stepper value
    [self.valueLabel setText:[NSString stringWithFormat:@"%d",(int)self.value]];
    
    if ([delegate respondsToSelector:@selector(cell:hasChangedDoubleValueTo:from:)]) {
        [delegate cell:self hasChangedDoubleValueTo:self.value from:oldValue];
    }
}

@end
