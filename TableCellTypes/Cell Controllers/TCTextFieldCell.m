//
//  TCTextFieldCell.m
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import "TCTextFieldCell.h"

@implementation TCTextFieldCell

@synthesize delegate, value, titleLabel, textField;

- (IBAction)textFieldValueChanged:(UITextField *)sender {
    NSString *oldValue = self.value;
    self.value = sender.text;
    if ([delegate respondsToSelector:@selector(cell:hasChangedStringValueTo:from:)]) {
        [delegate cell:self hasChangedStringValueTo:self.value from:oldValue];
    }
}

@end
