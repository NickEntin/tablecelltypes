//
//  TCSwitchCell.m
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import "TCSwitchCell.h"

@implementation TCSwitchCell

@synthesize delegate, value, titleLabel, switcher;

- (IBAction)switchValueChanged:(UISwitch *)sender {
    self.value = sender.isOn;
    if ([delegate respondsToSelector:@selector(cell:hasChangedBooleanValueTo:)]) {
        [delegate cell:self hasChangedBooleanValueTo:self.value];
    }
}

@end
