//
//  TCTextFieldCell.h
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TCTextFieldCellDelegate <NSObject>
@optional
- (void)cell:(UITableViewCell *)cell hasChangedStringValueTo:(NSString *)newValue from:(NSString *)oldValue;
@end

@interface TCTextFieldCell : UITableViewCell

// Class Properties
@property (nonatomic, retain) id<TCTextFieldCellDelegate>   delegate;
@property (nonatomic, assign) NSString                      *value;

// Interface Elements
@property (nonatomic, retain) IBOutlet UILabel              *titleLabel;
@property (nonatomic, retain) IBOutlet UITextField          *textField;

// Interface Methods
- (IBAction)textFieldValueChanged:(UITextField *)sender;

@end
