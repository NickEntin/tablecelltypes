//
//  TCSwitchCell.h
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TCSwitchCellDelegate <NSObject>
@optional
- (void)cell:(UITableViewCell *)cell hasChangedBooleanValueTo:(BOOL)newValue;
@end

@interface TCSwitchCell : UITableViewCell

// Class Properties
@property (nonatomic, retain) id<TCSwitchCellDelegate>      delegate;
@property (nonatomic, assign) BOOL                          value;

// Interface Elements
@property (nonatomic, retain) IBOutlet UILabel              *titleLabel;
@property (nonatomic, retain) IBOutlet UISwitch             *switcher;

// Interface Methods
- (IBAction)switchValueChanged:(UISwitch *)sender;

@end
