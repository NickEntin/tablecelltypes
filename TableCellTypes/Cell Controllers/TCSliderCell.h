//
//  TCSliderCell.h
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TCSliderCellDelegate <NSObject>
@optional
- (void)cell:(UITableViewCell *)cell hasChangedFloatValueTo:(float)newValue from:(float)oldValue;
@end

@interface TCSliderCell : UITableViewCell

// Class Properties
@property (nonatomic, retain) id<TCSliderCellDelegate>     delegate;
@property (nonatomic, assign) float                        value;

// Interface Elements
@property (nonatomic, retain) IBOutlet UILabel              *titleLabel;
@property (nonatomic, retain) IBOutlet UISlider             *slider;

// Interface Methods
- (IBAction)sliderValueChanged:(UISlider *)sender;

@end
