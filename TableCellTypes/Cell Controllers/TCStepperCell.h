//
//  TCStepperCell.h
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TCStepperCellDelegate <NSObject>
@optional
- (void)cell:(UITableViewCell *)cell hasChangedDoubleValueTo:(double)newValue from:(double)oldValue;
@end

@interface TCStepperCell : UITableViewCell

// Class Properties
@property (nonatomic, retain) id<TCStepperCellDelegate>     delegate;
@property (nonatomic, assign) double                        value;

// Interface Elements
@property (nonatomic, retain) IBOutlet UILabel              *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel              *valueLabel;
@property (nonatomic, retain) IBOutlet UIStepper            *stepper;


// Interface Methods
- (IBAction)stepperValueChanged:(UIStepper *)sender;

@end
