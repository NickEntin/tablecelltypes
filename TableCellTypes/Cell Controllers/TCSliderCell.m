//
//  TCSliderCell.m
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import "TCSliderCell.h"

@implementation TCSliderCell

@synthesize delegate, value, titleLabel, slider;

- (IBAction)sliderValueChanged:(UISlider *)sender {
    float oldValue = self.value;
    self.value = sender.value;
    if ([delegate respondsToSelector:@selector(cell:hasChangedFloatValueTo:from:)]) {
        [delegate cell:self hasChangedFloatValueTo:self.value from:oldValue];
    }
}

@end
