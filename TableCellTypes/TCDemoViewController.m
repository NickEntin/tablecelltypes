//
//  TCDemoViewController.m
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import "TCDemoViewController.h"

@interface TCDemoViewController ()

@end

@implementation TCDemoViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        identifiers = [NSArray arrayWithObjects:@"StepperCell", @"SliderCell", @"SwitchCell", @"SegmentedCell", @"TextFieldCell", nil];
    }
    return self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [identifiers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([identifiers[indexPath.row] isEqualToString:@"StepperCell"]) {
        TCStepperCell *cell = [tableView dequeueReusableCellWithIdentifier:identifiers[indexPath.row] forIndexPath:indexPath];
        [cell setDelegate:self];
        return cell;
    } else if ([identifiers[indexPath.row] isEqualToString:@"SliderCell"]) {
        TCSliderCell *cell = [tableView dequeueReusableCellWithIdentifier:identifiers[indexPath.row] forIndexPath:indexPath];
        [cell setDelegate:self];
        return cell;
    } else if ([identifiers[indexPath.row] isEqualToString:@"SwitchCell"]) {
        TCSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:identifiers[indexPath.row] forIndexPath:indexPath];
        [cell setDelegate:self];
        return cell;
    } else if ([identifiers[indexPath.row] isEqualToString:@"SegmentedCell"]) {
        TCSegmentedCell *cell = [tableView dequeueReusableCellWithIdentifier:identifiers[indexPath.row] forIndexPath:indexPath];
        [cell setDelegate:self];
        return cell;
    } else if ([identifiers[indexPath.row] isEqualToString:@"TextFieldCell"]) {
        TCTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:identifiers[indexPath.row] forIndexPath:indexPath];
        [cell setDelegate:self];
        return cell;
    } else if ([identifiers[indexPath.row] isEqualToString:@"MapCell"]) {
        TCMapCell *cell = [tableView dequeueReusableCellWithIdentifier:identifiers[indexPath.row] forIndexPath:indexPath];
        //[cell setDelegate:self];
        return cell;
    } else if ([identifiers[indexPath.row] isEqualToString:@"PagedCell"]) {
        TCPagedCell *cell = [tableView dequeueReusableCellWithIdentifier:identifiers[indexPath.row] forIndexPath:indexPath];
        // [cell setDelegate:self];
        return cell;
    }
    
    return nil;
}

#pragma mark - Custom cell delegate methods

- (void)cell:(UITableViewCell *)cell hasChangedDoubleValueTo:(double)newValue from:(double)oldValue {
    NSLog(@"Cell has double value to %f from %f",newValue,oldValue);
}
- (void)cell:(UITableViewCell *)cell hasChangedFloatValueTo:(float)newValue from:(float)oldValue {
    NSLog(@"Cell has float value to %f from %f",newValue,oldValue);
}
- (void)cell:(UITableViewCell *)cell hasChangedBooleanValueTo:(BOOL)newValue {
    NSLog(@"Cell has boolean value to %d",newValue);
}
- (void)cell:(UITableViewCell *)cell hasChangedIntValueTo:(int)newValue from:(int)oldValue {
    NSLog(@"Cell has int value to %d from %d",newValue,oldValue);
}
- (void)cell:(UITableViewCell *)cell hasChangedStringValueTo:(NSString *)newValue from:(NSString *)oldValue {
    NSLog(@"Cell has String value to %@ from %@",newValue,oldValue);
}

@end
