//
//  main.m
//  TableCellTypes
//
//  Created by Nick Entin on 8/9/13.
//  Copyright (c) 2013 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TCAppDelegate class]));
    }
}
