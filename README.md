TableCellTypes for iOS 6.1
==============

Collection of custom UITableViewCell classes and Storyboard cells

Cell controllers can be found in "TableViewCellTypes/Cell Controllers".  The following controllers are complete at this time:

+ TCStepperCell
+ TCSliderCell
+ TCSwitchCell
+ TCSegmentedCell
+ TCTextFieldCell

The Xcode project consists of a DemoViewController that currently only works on iPhone.